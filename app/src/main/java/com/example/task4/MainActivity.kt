package com.example.task4

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    private lateinit var resultTextView: TextView
    private lateinit var operator: String
    private var operand: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        resultTextView = findViewById(R.id.textView)
    }
    fun buttonClick(view:View){
        if(view is Button){

            var result = resultTextView.text.toString()
            if(result == "0"){
                result = ""
            }
            val button = view.text.toString()

            val concat = result + button

            resultTextView.text = concat
        }
    }
    fun operatorClick(view:View){
        if(view is Button){
            var operator = view.text.toString()
            this.operator = operator
            val result = resultTextView.text.toString()
            if(!result.isNullOrEmpty()){
                operand = result.toInt()
            }
            val concat = resultTextView.text.toString()
            resultTextView.text = concat + view.text.toString()
        }
    }
    fun equalsClick(view :View){
//        var secondOperand = resultTextView.text.toString()
        var secondOperand = operand.toString()
        var second = 0
        if(!secondOperand.isNullOrEmpty()){
            second = secondOperand.toInt()
        }
        when(operator){
            "+" -> resultTextView.text = (operand + second).toString()
            "-" -> resultTextView.text = (operand - second).toString()
            "*" -> resultTextView.text = (operand * second).toString()
            "/" -> resultTextView.text = (operand / second).toString()
        }

    }
    fun clearDislay(view: View){
        if(view is Button){
            resultTextView.text = ""
        }
    }
    fun decimalPoint(view: View){
        if(view is Button){
            var decimalPoint: String = view.text.toString()
        }
    }
}